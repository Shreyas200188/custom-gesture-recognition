# Custom Gesture Recognition
- Current systems allow only the pre-defined gestures,our product will allow the user to define their own custom gestures and use them for a custom task.

- Users should be able to control and manipulate the dynamic digital content by simply making their own customized gestures.

- **End Product : Desktop Application**


## Contributions:
- Anyone is welcomed to contribute
- Pick the domain you want to contribute in from the issues.
- Fork the project and start the work:-)
- Submit a Merge Request.
- Your code will get reviewed and then merged into project.        
- Feel free to add your own ideas in issues.


